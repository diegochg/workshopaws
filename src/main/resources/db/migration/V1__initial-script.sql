CREATE TABLE viewers(
  id bigint PRIMARY KEY,
  fullName varchar(40) NOT NULL,
  address varchar(40) NOT NULL,
  mail varchar(30) NOT NULL,
  bornDate TIMESTAMP NOT NULL,
  sex char(1) NOT NULL
);

CREATE SEQUENCE sq_viewers_id_pk
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9999999999
	START 1 NO CYCLE;

ALTER SEQUENCE sq_viewers_id_pk OWNED BY viewers.id;