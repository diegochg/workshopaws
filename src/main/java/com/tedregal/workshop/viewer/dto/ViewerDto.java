package com.tedregal.workshop.viewer.dto;

import java.util.Date;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ViewerDto {

  private String fullName;
  private String address;
  private String mail;
  private Date bornDate;
  private Character sex;

}
