package com.tedregal.workshop.viewer.service;

import com.tedregal.workshop.viewer.dto.ViewerDto;
import com.tedregal.workshop.viewer.model.Viewer;
import java.util.List;

public interface ViewerService {

  void saveViewer(ViewerDto viewerDto);

  List<Viewer> getAllViewers();

}
