package com.tedregal.workshop.viewer.service;

import com.tedregal.workshop.viewer.dto.ViewerDto;
import com.tedregal.workshop.viewer.model.Viewer;
import com.tedregal.workshop.viewer.repository.ViewerRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ViewerServiceImpl implements ViewerService {

  @Autowired
  private ViewerRepository viewerRepository;

  @Override
  public void saveViewer(ViewerDto viewerDto) {
    Viewer viewer = new Viewer();
    viewer.setFullName(viewerDto.getFullName());
    viewer.setAddress(viewerDto.getAddress());
    viewer.setMail(viewerDto.getMail());
    viewer.setBornDate(viewerDto.getBornDate());
    viewer.setSex(viewerDto.getSex());
    viewerRepository.save(viewer);
  }

  @Override
  public List<Viewer> getAllViewers() {
    return viewerRepository.findAll();
  }

}
