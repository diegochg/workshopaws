package com.tedregal.workshop.viewer.controller;

import com.tedregal.workshop.util.Message;
import com.tedregal.workshop.viewer.dto.ViewerDto;
import com.tedregal.workshop.viewer.model.Viewer;
import com.tedregal.workshop.viewer.service.ViewerService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/viewer")
public class ViewerController {

  @Autowired
  private ViewerService viewerService;

  @PostMapping("/create")
  public ResponseEntity<Message> saveViewer(@RequestBody ViewerDto viewerDto) {
    viewerService.saveViewer(viewerDto);
    return new ResponseEntity(new Message("Viewer successfully registered"), HttpStatus.CREATED);
  }

  @GetMapping("/all")
  public ResponseEntity<List<Viewer>> getAllViewers() {
    List<Viewer> viewerList = viewerService.getAllViewers();
    return new ResponseEntity(viewerList, HttpStatus.OK);
  }

}
