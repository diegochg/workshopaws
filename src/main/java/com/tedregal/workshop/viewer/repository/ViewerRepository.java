package com.tedregal.workshop.viewer.repository;

import com.tedregal.workshop.viewer.model.Viewer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ViewerRepository extends JpaRepository<Viewer, Long> {

}
