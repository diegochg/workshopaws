package com.tedregal.workshop.viewer.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "viewers")
public class Viewer implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
  @SequenceGenerator(sequenceName = "sq_viewers_id_pk", allocationSize = 1, name = "SEQ")
  @Column(name = "id")
  private Long id;

  private String fullName;
  private String address;
  private String mail;
  private Date bornDate;
  private Character sex;

}
