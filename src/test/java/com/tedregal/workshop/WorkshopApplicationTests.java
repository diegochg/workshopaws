package com.tedregal.workshop;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = WorkshopApplicationTests.class)
class WorkshopApplicationTests {

  @Test
  void contextLoads() {
  }

}
